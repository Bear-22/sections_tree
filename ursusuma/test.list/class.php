<?php

	use \Bitrix\Main\Loader;
	use \Bitrix\Main\Application;
	use Bitrix\Main\Localization\Loc;
	use Bitrix\Main\SystemException;
	use \Bitrix\Iblock\Iblock;
	use \Bitrix\Iblock\Model\Section;

	if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
		die();
	}

	class ExampleCompSimple extends CBitrixComponent {
		private $_request;

		/**
		 * Проверка наличия модулей требуемых для работы компонента
		 * @return bool
		 * @throws Exception
		 */
		private function _checkModules() {
			if (!Loader::includeModule('iblock') || !Loader::includeModule('sale')) {
				throw new \Exception('Не загружены модули необходимые для работы модуля');
			}

			return true;
		}


		/**
		 * Подготовка параметров компонента
		 * @param $arParams
		 * @return mixed
		 */
		public function onPrepareComponentParams($arParams) {
			// тут пишем логику обработки параметров, дополнение параметрами по умолчанию
			// и прочие нужные вещи
			//\Bitrix\Main\Diag\Debug::dump($arParams);
			$arParams["DEPTH_LEVEL"] = intval(["DEPTH_LEVEL"]);
			return $arParams;
		}

		/**
		 * Точка входа в компонент
		 * Должна содержать только последовательность вызовов вспомогательых ф-ий и минимум логики
		 * всю логику стараемся разносить по классам и методам
		 */
		public function executeComponent() {
			try {
				$this->_checkModules();
				if ($this->startResultCache()) {
					$this->arResult["ITEMS"] = $this->getSections();
					$this->IncludeComponentTemplate();
				}
			} catch (SystemException $e) {
				ShowError($e->getMessage());
			}
		}


		/**
		 * получение списка разделов с элементами в каждом разделе
		 * @return array
		 */
		protected function getSections() {
			$result   = [];
			$arrElements = $this->prepareArrElements();
			//если элементы в корне
			if (isset($arrElements[0])) {
				$result[0]["NAME"] = 'Без раздела';
				$result[0]["DEPTH_LEVEL"] = 0;
				$result[0]["ELEMENTS"] = $arrElements[0];
			}
			$entity    = Section::compileEntityByIblock($this->arParams['IBLOCK_ID']);
			$rsSection = $entity::getList([
				'order'  => ['LEFT_MARGIN' => 'ASC'],
				'filter' => [
					'ACTIVE' => 'Y',
				],
				'select' => [
					'ID',
					'NAME',
					'CODE',
					"IBLOCK_SECTION_ID",
					'DEPTH_LEVEL',
					'SECTION_PAGE_URL' => 'IBLOCK.SECTION_PAGE_URL',
				],
			]);
			while ($arSection = $rsSection->fetch()) {
				if (!isset($arrElements[$arSection['ID']])) {
					continue;
				}
				$arSection['ELEMENTS'] = $arrElements[$arSection['ID']];
				$arSection['SECTION_PAGE_URL']
									   = CIBlock::ReplaceDetailUrl($arSection['SECTION_PAGE_URL'], $arSection, true, 'S');
				$result[]              = $arSection;
			}


			return $result;
		}


		/**
		 * подготовка массива элементов сключом id сеции
		 * @return array
		 */
		protected function prepareArrElements() {
			$params = $this->arParams;

			$arrPropsCode = [];
			if (is_array($params["DISPLAY_PROPERTIES"]) && !empty($params["DISPLAY_PROPERTIES"])) {
				foreach ($params["DISPLAY_PROPERTIES"] as $item) {
					$arrPropsCode[] = $item;
				}
			}
			$select
						 = ['ID', 'NAME', 'CODE', "IBLOCK_SECTION_ID", 'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL'];
			$arrGetList
						 = [
				'filter' => ['ACTIVE' => "Y"],
				'order'  => ['sort' => 'ASC'],
			];
			$arrElements = $this->getElementIblock($params['IBLOCK_ID'], $arrGetList, $select, $arrPropsCode);
			$prepareArr  = [];

			foreach ($arrElements as $key => $item) {
				$item['DETAIL_PAGE_URL']
					= CIBlock::ReplaceDetailUrl($item['DETAIL_PAGE_URL'], $item, true, 'E');
				($item["IBLOCK_SECTION_ID"]) ?
					$prepareArr[$item["IBLOCK_SECTION_ID"]][$key] = $item
					: $prepareArr[0][$key] = $item;//если нет разделов
			}
			return $prepareArr;
		}

		/**
		 *
		 * @param int    $iblockId id инфоблока
		 * @param array  $arrPropName массив кодов свойств, если нет то выводяться все свойства
		 * @param string $separator разделитель имени для алиаса свойства
		 * @return array массивсвойств инфоблока
		 */
		protected function getArrPropIblock(int $iblockId, array $arrPropName = [], string $separator = '#') {
			$arrPropsName = [];
			if (!empty($arrPropName)) {
				$filter = ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y'];
				//если не передан массив свойств, то выводятся все своййства
				(!empty($arrPropName)) ? $filter['CODE'] = $arrPropName : null;
				$iblock = \Bitrix\Iblock\PropertyTable::getList([
					'filter' => $filter,
				]);
				while ($elem = $iblock->fetch()) {
					$arrPropsName[$elem['CODE']]          = $elem;
					$arrPropsName[$elem['CODE']]['ALIAS'] = $separator;
					switch ($elem['PROPERTY_TYPE']) {
						case 'S':
							$arrPropsName[$elem['CODE']]['ALIAS_CODE']    = $elem['CODE'];
							$arrPropsName[$elem['CODE']]['ALIAS_POSTFIX'] = null;
							$arrPropsName[$elem['CODE']]['ALIAS']         .= $elem['CODE'];
							break;
						case 'L':
							$arrPropsName[$elem['CODE']]['ALIAS_CODE']    = $elem['CODE'] . '.ITEM';
							$arrPropsName[$elem['CODE']]['ALIAS_POSTFIX'] = 'ITEM';
							$arrPropsName[$elem['CODE']]['ALIAS']         .= $elem['CODE'];
							break;
						case 'E':
							$arrPropsName[$elem['CODE']]['ALIAS_CODE']    = $elem['CODE'] . '.ELEMENT';
							$arrPropsName[$elem['CODE']]['ALIAS_POSTFIX'] = 'ELEMENT';
							$arrPropsName[$elem['CODE']]['ALIAS']         .= $elem['CODE'];
							break;
						case 'F':
							$arrPropsName[$elem['CODE']]['ALIAS_CODE']    = $elem['CODE'] . '.FILE';
							$arrPropsName[$elem['CODE']]['ALIAS_POSTFIX'] = 'FILE';
							$arrPropsName[$elem['CODE']]['ALIAS']         .= $elem['CODE'];
							break;
						case 'G':
							$arrPropsName[$elem['CODE']]['ALIAS_CODE']    = $elem['CODE'] . '.SECTION';
							$arrPropsName[$elem['CODE']]['ALIAS_POSTFIX'] = 'SECTION';
							$arrPropsName[$elem['CODE']]['ALIAS']         .= $elem['CODE'];
							break;
						default:
							break;
					}
					$arrPropsName[$elem['CODE']]['ALIAS'] .= $separator;
				}
			}

			return $arrPropsName;
		}

		/**
		 * обработчик возвращает множественные свойства в одном массиве
		 * @param int            $iblockId id инфоблока
		 * @param array          $arrGetList основной массив getList без параметра select
		 * @param array|string[] $select основные поля select без свойств по умолчанию полная выборка
		 * @param array          $arrPropName перечеь кодов для select без добавления префиксов
		 * @param string         $separator разделитель имени для алиаса свойства
		 * @return array пустой массив либо выборку с элементами и их свойствами
		 */

		protected function getElementIblock(int $iblockId, array $arrGetList = [], array $select = ['*'],
											array $arrPropName = [],
											string $separator = '#') {
			$class = \Bitrix\Iblock\Iblock::wakeUp($iblockId)->getEntityDataClass();
			if (!empty($arrPropName)) {
				//получаем все элемент по его id(SEGMENT.ITEM  своййство тип список)
				$propFields = $this->getArrPropIblock($iblockId, $arrPropName);

				foreach ($propFields as $prop) {
					$select[$prop['ALIAS']] = $prop['ALIAS_CODE'];
				}
			}
			$arrGetList['select'] = $select;
			$elements             = $class::getList($arrGetList)->fetchAll();
			$res                  = [];
			if (is_array($elements) && !empty($elements)) {
				foreach ($elements as $index => $item) {

					foreach ($item as $key => $row) {
						//тсключаем проход по уже существующим полям
						if (isset($res[$item['ID']][$key])) {
							continue;
						}
						//проверяем на то что это свойство
						if (substr($key, 0, 1) != $separator) {
							$res[$item['ID']][$key] = $row;
						} else {
							//если это свойство
							preg_match('/' . $separator . '(.*?)' . $separator . '(.*)/', $key, $matches);
							$prop_name = &$res[$item['ID']]['PROP'][$matches[1]];
							//название свойства
							$prop_name['NAME'] = $propFields[$matches[1]]['NAME'];
							//если свойство множественное
							if ($propFields[$matches[1]]['MULTIPLE'] == 'Y') {
								switch ($propFields[$matches[1]]['ALIAS_POSTFIX']) {
									//обработка связаных элементов
									case 'ELEMENT':
									case 'FILE':
									case 'SECTION':
										//ключ ID
										$id = $separator . $matches[1] . $separator . 'ID';
										//если нет усвойства нет занчения id и нет такого свойства по ключу то добавляем
										if (isset($item[$id]) && !isset($prop_name[$item[$id]][$key])) {
											$prop_name[$item[$id]][$matches[2]] = $row;
										}
										break;
									//обработка списков
									case 'ITEM':
										if ($matches[2] == 'VALUE') {
											//ключ XML_ID
											$xml_id = $separator . $matches[1] . $separator . 'XML_ID';
											//проверяем есть ли уже свойство с уникальным XML_ID
											if (!isset($prop_name['XML_ID'][$item[$xml_id]])) {
												$prop_name['XML_ID'][$item[$xml_id]] = $row;
												//ключ SORT
												$sort = $separator . $matches[1] . $separator . 'SORT';
												//необходимо привести построение в соответсвии с сортировкой из админки
												$sort_key
													= (isset($prop_name['VALUE'][$item[$sort]]))
													? intval($item[$sort]) + count($prop_name['VALUE'])
													: intval($item[$sort]);

												$prop_name['VALUE'][$sort_key] = $row;
												ksort($prop_name['VALUE']);
											}
										}
										if (!isset($prop_name[$matches[2]])) {
											$prop_name[$matches[2]] = $row;
										}
										break;
									default:
										break;
								}

							} elseif (!isset($prop_name[$matches[2]])) {
								//если не множественное, то один раз добавляем и больше понему не проходим
								$prop_name[$matches[2]] = $row;
							}

						}

					}
				}

			}
			return $res;
		}
	}
