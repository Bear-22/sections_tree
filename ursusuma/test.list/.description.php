<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

	use Bitrix\Main\Localization\Loc;

	$arComponentDescription = [
		"NAME" => Loc::getMessage("TEST_NAME"), "DESCRIPTION" => Loc::getMessage("TEST_DESCRIPTION"), "PATH" => [
			"ID" => Loc::getMessage("TEST_ID"),
		],

	];
?>


