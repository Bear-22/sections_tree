<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();

	$MESS["TEST_SETTINGS"]    = "Выбор инфоблока и разделов";
	$MESS["TEST_IBLOCK_TYPE"] = "Тип инфоблока";
	$MESS["TEST_IBLOCK_ID"]   = "Инфоблок";
	$MESS["TEST_SECTION_IDS"] = "ID разделов через запятую";
	$MESS["TEST_PROP"] = "Свойства инфоблока";
