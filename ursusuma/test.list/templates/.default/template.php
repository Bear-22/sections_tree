<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);
?>

<div class="category">
	<? foreach ($arResult["ITEMS"] as $arItem): ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
		?>
		<? //разделы ?>
		<? if ($arItem["NAME"]): ?>
            <a href="<?= $arItem['SECTION_PAGE_URL']; ?>"
               class="category__section pl-<?= $arItem["DEPTH_LEVEL"] ?>"
               id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
				<?= $arItem["NAME"] ?> (Вложенность - <?= $arItem["DEPTH_LEVEL"] ?>)
            </a>
		<? endif; ?>

		<? //элементы разделов ?>
		<? if (isset($arItem["ELEMENTS"]) && !empty($arItem["ELEMENTS"])): ?>
			<? foreach ($arItem["ELEMENTS"] as $key => $item): ?>
                <a href="<?= $item['DETAIL_PAGE_URL']; ?>"
                   class="category__element pl-<?= $arItem["DEPTH_LEVEL"] ?>">
					<?= $item["NAME"] ?>

					<? //свойстава?>
					<? if (isset($item["PROP"]) && !empty($item["PROP"])): ?>
						<? foreach ($item["PROP"] as $prop): ?>
							<? if (isset($prop["VALUE"])): ?>
                                <div><?=$prop["NAME"]; ?> </div>
                                <ul>
									<? if (is_array($prop["VALUE"])): ?>
										<? foreach ($prop["VALUE"] as $list): ?>
                                            <li><?= $list; ?> </li>
										<? endforeach; ?>
									<? else: ?>
                                        <li><?= $prop["VALUE"] ?> </li>
									<? endif; ?>
                                </ul>
							<? endif; ?>

						<? endforeach; ?>
					<? endif; ?>

                </a>
			<? endforeach; ?>
		<? endif; ?>

	<? endforeach; ?>
</div>


