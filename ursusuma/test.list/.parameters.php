<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
		die();
	}

	/**
	 * @var string $componentPath
	 * @var string $componentName
	 * @var array  $arCurrentValues
	 * */

	use Bitrix\Main\Loader;
	use Bitrix\Main\Localization\Loc;
	use \Bitrix\Main\Context;
	use \Bitrix\Iblock\IblockTable;
	use \Bitrix\Iblock\TypeTable;
	use \Bitrix\Iblock\PropertyTable;

	if (!Loader::includeModule("iblock")) {
		throw new \Exception('Не загружены модули необходимые для работы компонента');
	}

	$context = Context::getCurrent();
	$langId  = $context->getLanguage();

	// типы инфоблоков
	$arIBlockType    = [];
	$arIBlockTypeRes = TypeTable::getList([
		'select' => ['ID', 'MESS_' => "LANG_MESSAGE"],
	])->fetchAll();
	foreach ($arIBlockTypeRes as $type) {
		//получаем названия типов инфоблоков соотвественно языку сайта
		if ($type["MESS_LANGUAGE_ID"] == $langId) {
			$arIBlockType[$type['ID']] = "[{$type['ID']}] {$type['MESS_NAME']}";
		}
	}

	// инфоблоки выбранного типа
	$arIBlock     = [];
	$iblockFilter = !empty($arCurrentValues['IBLOCK_TYPE'])
		? ['IBLOCK_TYPE_ID' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y']
		: ['ACTIVE' => 'Y'];

	$rsIBlock = IblockTable::getList([
		'select' => ['ID', 'NAME', 'IBLOCK_TYPE_ID', 'ACTIVE'],
		'filter' => [$iblockFilter]
	])->fetchAll();
	foreach ($rsIBlock as $iblock) {
		$arIBlock[$iblock['ID']][] = "[{$iblock['ID']}] {$iblock['NAME']}";
	}

	//свойства инфоблока
	$arPropsRes = PropertyTable::getList([
		'select' => ['ID', 'NAME', 'MULTIPLE', 'CODE','IBLOCK_ID','ACTIVE'],
		'filter' => ['IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], 'ACTIVE' => 'Y'],
	])->fetchAll();
	foreach ($arPropsRes as $prop) {
		$arProps[$prop['CODE']][] = "[{$prop['ID']}] {$prop['NAME']}";
	}
	unset($rsIBlock, $iblockFilter, $arIBlockTypeRes, $arPropsRes);

	$arComponentParameters = [
		// группы в левой части окна
		"GROUPS"     => [
			"SETTINGS" => [
				"NAME" => Loc::getMessage('TEST_SETTINGS'),
				"SORT" => 550,
			],
		],
		// поля для ввода параметров в правой части
		"PARAMETERS" => [
			// Произвольный параметр типа СПИСОК
			"IBLOCK_TYPE"        => [
				"PARENT"            => "SETTINGS",
				"NAME"              => Loc::getMessage('TEST_IBLOCK_TYPE'),
				"TYPE"              => "LIST",
				"ADDITIONAL_VALUES" => "Y",
				"VALUES"            => $arIBlockType,
				"REFRESH"           => "Y",
				'MULTIPLE'          => 'N',
			],
			"IBLOCK_ID"          => [
				"PARENT"            => "SETTINGS",
				"NAME"              => Loc::getMessage('TEST_IBLOCK_ID'),
				"TYPE"              => "LIST",
				"ADDITIONAL_VALUES" => "Y",
				"VALUES"            => $arIBlock,
				"REFRESH"           => "Y",
				'MULTIPLE'          => 'N',
			],
			"DISPLAY_PROPERTIES" => [
				"PARENT"            => "SETTINGS",
				"NAME"              => Loc::getMessage('TEST_PROP'),
				"TYPE"              => "LIST",
				"ADDITIONAL_VALUES" => "N",
				"VALUES"            => $arProps,
				"REFRESH"           => "N",
				'MULTIPLE'          => 'Y',
			],
			// Произвольный параметр типа СТРОКА
/*			"SECTION_IDS"        => [
				"PARENT"   => "SETTINGS",
				"NAME"     => Loc::getMessage('TEST_SECTION_IDS'),
				"TYPE"     => "STRING",
				"MULTIPLE" => "N",
				"DEFAULT"  => "",
				"COLS"     => 25,
			],*/
			// Настройки кэширования
			'CACHE_TIME'         => ['DEFAULT' => 3600],
		],
	];
